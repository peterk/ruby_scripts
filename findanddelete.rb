#!/usr/bin/ruby -w
#
#usage information
def usage(s)
    $stderr.puts(s)
    $stderr.puts("Usage: #{File.basename($0)}: [-p path] [-l logfile] [-q] file ")
    exit(2)
end

#defaults
quiet = false
logfile = false
path = '.'

loop {
    case ARGV[0]
        when '-p' then ARGV.shift; path = ARGV.shift;
        when '-q' then ARGV.shift; quiet = true;
        when '-l' then ARGV.shift; logfile = ARGV.shift;
        when /^-/ then usage("Unknown option: #{ARGV[0].inspect}")
        else break
    end;
}
if !ARGV[0] then
    usage('Please specify a filename to delete');
    exit(2)
end

filename = ARGV[0]
filekiller = Object.new()

class << filekiller
    attr_accessor :filename
    attr_accessor :path
    attr_accessor :logfile
    attr_accessor :quiet

    def search_and_destroy
        puts "searching recursively for filename #{@filename} starting path #{@path}"
        found = 0
        deleted = 0
        failed = 0
        Dir.glob("**/#{@filename}") do |filename|
            found += 1
            if self.confirmdelete(filename) then
                begin
                    File.delete filename
                    deleted += 1
                    msg = 'DELETED ' + filename
                rescue Exception => e
                    puts e.message
                    msg = "FAILED " + e.message
                    failed += 1
                end
                self.logevent(msg)
            end
        end
        if !@quiet then
            puts "found   " + found.to_s()
            puts "deleted " + deleted.to_s()
            puts "failed " + failed.to_s()
        end
    end

    #logs a message to logfile
    def logevent(msg)
        if !@quiet then
            puts msg
        end
        if @logfile then
            file = File.open(@logfile, "a")
            time = Time.now
            file.write(time.inspect + " " + msg + "\n")
            file.close
        end
    end

    #asks you to confirm the deletion
    def confirmdelete(filename)
        delete = false
        answer = "n"
        if !@quiet then
            puts "delete ./" + @filename + "? (y/N)"
            print '>'
            answer = STDIN.gets.chomp()
        end
        if answer.capitalize.eql?("Y") or quiet
            delete = true
        end
        return delete
    end
end

filekiller.filename = filename
filekiller.logfile = logfile
filekiller.quiet = quiet
filekiller.path = path
filekiller.search_and_destroy
